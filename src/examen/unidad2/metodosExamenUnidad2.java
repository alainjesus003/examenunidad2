package examen.unidad2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author REDES
 */
public class metodosExamenUnidad2 {
    private int codigoV;
    private int cantidad;
    private int tipo;
    
    public metodosExamenUnidad2(){
    this.codigoV = 0;
    this.cantidad = 0;
    this.tipo = 0 ;
}

    public metodosExamenUnidad2(int codigoV, int cantidad, int tipo) {
        this.codigoV = codigoV;
        this.cantidad = cantidad;
        this.tipo = tipo;
    }
    
    public metodosExamenUnidad2(metodosExamenUnidad2 otro) {
        this.codigoV = otro.codigoV;
        this.cantidad = otro.cantidad;
        this.tipo = otro.tipo;
    }

    public int getCodigoV() {
        return codigoV;
    }

    public void setCodigoV(int codigoV) {
        this.codigoV = codigoV;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    //METODOS DE COMPORTAMIENTO
    
    public float precio(){
        float precio=0.0f;
        if(this.tipo==1){
            this.tipo=1;
           precio=24.50f;
        }else if (this.tipo==2){
            this.tipo=2;
            precio=20.5f;
        }
        return precio;
    }
    
    public float total(){
        float total=0.0f;
        total=this.cantidad*this.precio();
        return total;
    }
    
}
